from django.urls import path
from .views import viewMain, viewUser, viewUserSettings, viewUserRegister, viewDeleteUser, viewGames, viewGameContent, viewGameRegister, viewDeleteGame, viewGameSettings, viewBetRegister, viewBets, viewBet


urlpatterns = [
    path("usuarios/", viewMain, name = "usuarios"),
    path("user/<str:userName>/", viewUser, name = "user"),
    path("configuracoes/<str:userName>/", viewUserSettings, name = "settings"),
    path("registro/", viewUserRegister, name = "register"),
    path("deletar/<str:userName>/", viewDeleteUser, name = "delete"),
    path("principal/", viewGames, name = "partidas" ),
    path("partidas/<int:id>/", viewGameContent, name = "partida"),
    path("registro-jogos/", viewGameRegister, name="registroJogos"),
    path("deletar-partida/<int:id>", viewDeleteGame, name="deletarPartida"),
    path("alterar-partida/<int:id>/", viewGameSettings, name = "gameSettings"),
    path("bets/", viewBets, name= "bets"),
    path("registro-bets/", viewBetRegister, name= "registroBets"),
    path("bets/<int:id>/", viewBet, name='bet'),

]   
