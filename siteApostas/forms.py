from django.forms import ModelForm, TextInput, EmailInput, NumberInput, ChoiceField
from .models import Bet, Game, User


class userRegister(ModelForm):
    class Meta:
        model = User
        fields = ['userName', 'firstName', 'lastName', 'email', 'cpf', 'balance']
        widgets = {
            'userName': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Nome de Usuario',}),
            'firstName': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Nome'}),
            'lastName': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Sobrenome'}),
            'email': EmailInput(attrs={
                'class': 'form-control',
                'placeholder': 'email'}),
            'cpf': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'cpf'}),
            'balance': NumberInput(attrs={
                'class': 'form-control',
                'placeholder': 'saldo'}),
        }

class gameRegister(ModelForm):
    class Meta:
        model = Game
        fields = ['homeTeam', 'awayTeam', 'h_ratio', 'a_ratio', 'winner']
        """ widgets = {
            'homeTeam': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Nome do time',}),
            'awayTeam': TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Nome do time'}),
            'h_ratio': NumberInput(attrs={
                'class': 'form-control',
                'placeholder': 'odd'}),
            'a_ratio': NumberInput(attrs={
                'class': 'form-control',
                'placeholder': 'odd'})

        } """

class betRegister(ModelForm):
    class Meta:
        model = Bet
        fields = ['userName', 'game', 'value', 'side']  