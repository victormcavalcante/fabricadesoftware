## Desafio Fabrica de Software

## Descrição e funcionalidades
No desafio de backend da Fabrica de Software, foi solicitado aos alunos que fizessem um site, simples, em, aproximadamente 24 horas, seguindo como referencia o blog feito no workshop, porem com algumas funcionalidades a mais. Decidi então por fazer a base de um site de apostas, na qual pode-se cadastrar usuário, o mesmo tendo username, primeiro nome, ultimo nome, email, cpf, e o valor aportado que o usuario gostaria de colocar no site, tambem é possivel alterar informações do mesmo e deletar-lo caso necessário. Também coloquei o cadastro de partidas, na qual partidas podem ser cadastradas indicando a data, os times que se enfrentarão, as odds (caso ja tenham havido apostas) na qual foi também efetuado um sistema de apostas, no qual envolve um simples calculo(das odds) apenas para fins demonstrativos da funcionalidade, alem de que a partir do resultado da partida, alem de todas as apostas sumirem, o usuario tem na sua banca, o valor acrescentado, caso ganhe a aposta, e o valor retirado assim que a aposta foi efetuada, e caso perca, o dinheiro não volta ao usuário. Para selecionar o vencedor da partida, após a mesma ter sido finalizada, o usuario deve modificar a partida, na aba "modificar" e escolher entre 1 e 2 o vencedor da partida (sendo 1 o time da casa e 2 o visitante).  

### Calculo de Odds

Com o objetivo de simplificar, o calculo de Odds(Probabilidade e recompensa) foi feito da seguinte maneira: a cada aposta realizada, a Odd do time em que foi apostado é decrescida em 0.1 (tendo um minimo de 1.01), e a Odd do time oposto ao apostado tem um acrescimo de 0.1 (não tendo limite máximo). 

A recompensa é valorApostado * Odd

## Instalação
```
git clone https://gitlab.com/victormcavalcante/fabricadesoftware.git
pip install -r requirements.txt

```
## Executar

```
python manage.py runserver
```

acessar navegador localhost:8000

## Uso

### Criar Usuario
* Clicar em `ver usuarios cadastrados`
* Clicar em `Cadastrar Usuario`
* Preencher os campos requiridos
* Clicar em Salvar

### Atualizar Usuario

* Clicar em `Informaçoes do Usuário` do usuario que deseja alterar
* Clicar em `Alterar Usuário`
* Alterar o campo que deseja
* Clicar em Salvar

### Excluir Usuario

* Clicar em `Informaçoes do Usuário` do usuario que deseja excluir
* Clicar em `Excluir Usuario`

## Partida

### Cadastrar partida

* Clicar em `Registrar nova partida`
* Preencher os campos
* Clicar em Salvar

### Modificar Partida

* Clicar em `Informaçoes da partida`
* Clicar em `Modificar Partida`
* Alterar os campos necessários

> Obs: Selecionar o Vencedor da Partida ira encerrar a partida e todas as apostas relacionadas a ela. Isso implica na deleçao dos objetos no banco de dados e na alteração do saldo na conta dos usuarios

### Excluir Partida

* Clicar em `Informaçoes da partida`
* Clicar em `Excluir Partida`

## Aposta

### Efetuar Aposta

* Clicar em `Efetuar Aposta`
* Preencher os campos necessarios
* Clicar em Salvar

### Ver Apostas

* Clicar em `Ver Apostas` para ver todas as apostas existentes  
* Caso queira ver alguma em expecifico, clicar em `Ver Aposta`

