from django.shortcuts import render, redirect, get_object_or_404

from siteApostas.forms import userRegister, gameRegister, betRegister
from .models import Bet, User, Game
# Create your views here.

def viewMain(request):
    users = User.objects.all()
    return render(request, "usuarios.html", {"users":users})

def viewUser(request, userName):
    users = User.objects.all()
    searchedUser = get_object_or_404(User, pk=userName)
    return render(request, "user.html", {"searchedUser":searchedUser, "userName":userName})

def viewUserSettings(request, userName):
    changedUser = get_object_or_404(User, pk=userName)
    formulario = userRegister(request.POST or None, instance=changedUser)

    if formulario.is_valid():
        formulario.save()
        return redirect("usuarios")
    return render(request, "userSettings.html", {"formulario":formulario})

def viewUserRegister(request):
    formulario = userRegister(request.POST, request.FILES,)
    if formulario.is_valid():
        formulario.save()
        return redirect("usuarios")
    return render(request, "register.html", {"formulario":formulario})

def viewDeleteUser(request, userName):
    
    deletedUser = get_object_or_404(User, pk=userName)
    deletedUser.delete()

    return redirect("usuarios")

def viewGames(request):
    games = Game.objects.all()
    return render(request, "principal.html", {"games":games})

def viewGameRegister(request):
    formulario = gameRegister(request.POST, request.FILES,)
    if formulario.is_valid():
        formulario.save()
        return redirect("partidas")
    return render(request, "gameRegister.html", {"formulario":formulario})

def viewGameContent(request, id):
    games = Game.objects.all()
    researchedGame = get_object_or_404(Game, pk=id)
    return render(request, "gameContent.html", {"researchedGame":researchedGame, "id":id})


def viewDeleteGame(request, id):
    deletedGame = get_object_or_404(Game, pk=id)
    deletedGame.delete()
    return redirect("partidas")


def viewGameSettings(request, id):
    changedGame = get_object_or_404(Game, pk=id)
    formulario = gameRegister(request.POST or None, instance=changedGame)
    if formulario.is_valid():
        if request.POST["winner"] != "":            
            bets = Bet.objects.filter(game = changedGame)
            for bet in bets:
                user = User.objects.get(pk=bet.userName)
                if request.POST["winner"] == bet.side:
                    if bet.side == "H":
                        user.balance += bet.value * changedGame.h_ratio
                    else:
                        user.balance += bet.value * changedGame.a_ratio
                    user.save()
                bet.delete()
            changedGame.delete()
            return redirect("partidas")
        formulario.save()
        return redirect("partidas")
    return render(request, "gameSettings.html", {"formulario":formulario})


def viewBets(request):
    bets = Bet.objects.all()
    return render(request, "allBets.html", {"bets":bets})


def viewBetRegister(request):
    formulario = betRegister(request.POST, request.FILES,)
    if formulario.is_valid():
        user = User.objects.get(pk = request.POST["userName"])
        user.balance = user.balance - float(request.POST["value"])
        user.save()
        
        game = Game.objects.get(pk=request.POST["game"])
        if request.POST["side"] == "H":
            game.h_ratio += 0.1
            game.a_ratio = max(1.01, game.a_ratio - 0.1)
        else:
            game.a_ratio += 0.1
            game.h_ratio = max(1.01, game.h_ratio - 0.1)
        game.save()    
        formulario.save()

        return redirect("partidas")
    return render(request, "betRegister.html", {"formulario":formulario})

def viewBet(request, id):
    bets = Bet.objects.all()
    researchedBet = get_object_or_404(Bet, pk=id)
    return render(request, "bet.html", {"researchedBet":researchedBet, "id":id})