from django.db import models

CHOICES = [
    ('H', '1'),
    ('A', '2')
] 
#Usuario vai ter nome, primeiro nome, ultimo nome, email, cpf, saldo

class User(models.Model):
    userName = models.CharField(max_length=80, blank=False, primary_key=True)
    firstName = models.CharField(max_length=20, blank=False)
    lastName = models.CharField(max_length=30, blank=False)
    email = models.CharField(max_length=50, blank=False)
    cpf = models.CharField(max_length=14, blank=False)
    balance = models.FloatField(blank= False)

    def __str__(self):
        return self.userName

class Game(models.Model):
    homeTeam = models.CharField(max_length=20, blank=False)
    awayTeam = models.CharField(max_length=20, blank=False)
    h_ratio = models.FloatField(blank=False)
    a_ratio = models.FloatField(blank=False)
    matchDay = models.DateTimeField(auto_now_add=True, verbose_name="Data da partida")
    winner = models.CharField(max_length=3, blank=True, choices=CHOICES)

    def __str__(self):
        return self.homeTeam + ' x ' + self.awayTeam

class Bet(models.Model):
    userName = models.ForeignKey(User, on_delete=models.CASCADE)
    game = models.ForeignKey(Game,on_delete=models.CASCADE)
    value = models.FloatField(blank=False)
    side = models.CharField(max_length=3, blank=False, choices=CHOICES)

    def __str__(self):
        return str(self.userName) + " " + str(self.game)
